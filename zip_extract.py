import zipfile #zipfile library
import shutil  #to handle files in the mfolder
from configparser import SafeConfigParser
import time
from datetime import date, timedelta

#loading the config file data initially
start_time = time.time()
zip_parser = SafeConfigParser()
zip_parser.read('zip_extract.ini')

#extracting the zip files
print ("Extracting LACS zip file \n")
zip_ref = zipfile.ZipFile("C:\\Users\\mekala01\\Downloads\\amith\\lacs.zip", 'r')
zip_ref.extractall("C:\\Users\\mekala01\\Downloads\\amith\\lacs")
print ("Extracting LACS zip file - COMPLETED \n")

print ("Extracting DPV zip file \n")
zip_ref = zipfile.ZipFile("C:\\Users\\mekala01\\Downloads\\amith\\dpv.zip", 'r')
zip_ref.extractall("C:\\Users\\mekala01\\Downloads\\amith\\dpv")
print ("Extracting DPV zip file - COMPLETED \n")

print ("Extracting Tom_tom zip file \n")
zip_ref = zipfile.ZipFile("C:\\Users\\mekala01\\Downloads\\amith\\tom_tom.zip", 'r')
zip_ref.extractall("C:\\Users\\mekala01\\Downloads\\amith\\tom_tom")
print ("Extracting Tom_tom zip file - COMPLETED \n")

print ("Extracting EWS zip file \n")
zip_ref = zipfile.ZipFile("C:\\Users\\mekala01\\Downloads\\amith\\ews.zip", 'r')
zip_ref.extractall("C:\\Users\\mekala01\\Downloads\\amith\\ews")
print ("Extracting EWS zip file - COMPLETED \n")

zip_ref.close()

print ("Copying required files to FINAL FOLDER\n")
#copying the required files to the final directory.
#dpv db
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\dpv\\dpvh.db', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
#lacs db
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\lacs\\llk.db', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
#tom_tom files
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\alaska.las', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\alaska.los', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\cbsac.dir', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\Census2k.dld', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\CityCost.dld', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\conus.las', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\conus.los', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\counties.gsa', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\counties.gsb', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\ctyst.dir', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\dpsa.tomtom', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\finmbr.dat', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\msabyzip.txt', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\parse.dir', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\prvi.las', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\prvi.los', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\states.gsa', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\states.gsb', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\us.gsd', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\us.gsi', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\us.gsl', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\us.gsz', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\us.z9', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\usps.gdi', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\ust.gsd', 'C:\\Users\\mekala01\\Downloads\\amith\\final')
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\tom_tom\\zip9.gsu', 'C:\\Users\\mekala01\\Downloads\\amith\\final')

#renaming the out file to ews.txt
shutil.copy2('C:\\Users\\mekala01\\Downloads\\amith\\ews\\CIIMPN\\PS124D01\\EWS002C0\\HEADER\\OUT', 'C:\\Users\\mekala01\\Downloads\\amith\\final\\ews.txt')

print ("Files copying done \n")
print("--- %s minutes for Extracting and copy---" % str(int(time.time() - start_time)/60))


"""
for i in [1,2,3]:
    # constructing the file_key
    zip_file_key = 'file' + str(i)
    file_key = 'file' + str(i) 
    #getting the file name from key
    zip_file_name = zip_parser.get('zip_file_names', zip_file_key)
    file_name = zip_parser.get('file_names', file_key)
    #constructing the file path
    final_zip_file_path = zip_file_path + "\\\\" + zip_file_name
    final_file_path = extract_zip_file_path + "\\\\" + file_name
    print (str(i) +" "+ final_zip_file_path)
    print (str(i) + " "+ final_file_path)
    zip_ref = zipfile.ZipFile(final_zip_file_path, 'r')
    zip_ref.extractall(final_file_path)

zip_ref.close()
"""
